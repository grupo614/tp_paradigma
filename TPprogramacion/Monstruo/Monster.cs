﻿using System;

namespace Monstruo
{
    public class Monster
    {
        public string Nombre { get; set; }
        public enum tipos{fuego, planta, agua}
        public tipos Tipo;
            
        public int Velocidad { get; set; }
        public int PoderAtaque { get; set; }
        public int Defensa { get; set; }
        public int Vida { get; set; }
    }
}
